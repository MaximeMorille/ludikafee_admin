'use strict'

describe 'Service: persons', ->

  # load the service's module
  beforeEach module 'ludikafeeAdminApp'

  # instantiate service
  persons = {}
  beforeEach inject (_persons_) ->
    persons = _persons_

  it 'should do something', ->
    expect(!!persons).toBe true
