'use strict'

describe 'Service: apiKey', ->

  # load the service's module
  beforeEach module 'ludikafeeAdminApp'

  # instantiate service
  apiKey = {}
  beforeEach inject (_apiKey_) ->
    apiKey = _apiKey_

  it 'should do something', ->
    expect(!!apiKey).toBe true
