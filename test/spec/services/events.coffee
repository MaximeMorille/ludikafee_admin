'use strict'

describe 'Service: events', ->

  # load the service's module
  beforeEach module 'ludikafeeAdminApp'

  # instantiate service
  events = {}
  beforeEach inject (_events_) ->
    events = _events_

  it 'should do something', ->
    expect(!!events).toBe true
