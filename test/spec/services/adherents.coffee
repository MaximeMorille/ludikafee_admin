'use strict'

describe 'Service: adherents', ->

  # load the service's module
  beforeEach module 'ludikafeeAdminApp'

  # instantiate service
  adherents = {}
  beforeEach inject (_adherents_) ->
    adherents = _adherents_

  it 'should do something', ->
    expect(!!adherents).toBe true
