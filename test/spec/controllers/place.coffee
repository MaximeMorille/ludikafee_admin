'use strict'

describe 'Controller: PlaceCtrl', ->

  # load the controller's module
  beforeEach module 'ludikafeeAdminApp'

  PlaceCtrl = {}

  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    PlaceCtrl = $controller 'PlaceCtrl', {
      # place here mocked dependencies
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(PlaceCtrl.awesomeThings.length).toBe 3
