'use strict'

describe 'Controller: AdherentAdherentCreateCtrl', ->

  # load the controller's module
  beforeEach module 'ludikafeeAdminApp'

  AdherentAdherentCreateCtrl = {}

  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    AdherentAdherentCreateCtrl = $controller 'AdherentAdherentCreateCtrl', {
      # place here mocked dependencies
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(AdherentAdherentCreateCtrl.awesomeThings.length).toBe 3
