'use strict'

describe 'Controller: AdherentAdherentUpdateCtrl', ->

  # load the controller's module
  beforeEach module 'ludikafeeAdminApp'

  AdherentAdherentUpdateCtrl = {}

  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    AdherentAdherentUpdateCtrl = $controller 'AdherentAdherentUpdateCtrl', {
      # place here mocked dependencies
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(AdherentAdherentUpdateCtrl.awesomeThings.length).toBe 3
