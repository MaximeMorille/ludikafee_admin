'use strict'

describe 'Controller: EventEventUpdateCtrl', ->

  # load the controller's module
  beforeEach module 'ludikafeeAdminApp'

  EventEventUpdateCtrl = {}

  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    EventEventUpdateCtrl = $controller 'EventEventUpdateCtrl', {
      # place here mocked dependencies
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(EventEventUpdateCtrl.awesomeThings.length).toBe 3
