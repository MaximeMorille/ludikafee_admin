'use strict'

describe 'Controller: EventEventCreateCtrl', ->

  # load the controller's module
  beforeEach module 'ludikafeeAdminApp'

  EventEventCreateCtrl = {}

  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    EventEventCreateCtrl = $controller 'EventEventCreateCtrl', {
      # place here mocked dependencies
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(EventEventCreateCtrl.awesomeThings.length).toBe 3
