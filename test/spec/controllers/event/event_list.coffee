'use strict'

describe 'Controller: EventEventListCtrl', ->

  # load the controller's module
  beforeEach module 'ludikafeeAdminApp'

  EventEventListCtrl = {}

  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    EventEventListCtrl = $controller 'EventEventListCtrl', {
      # place here mocked dependencies
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(EventEventListCtrl.awesomeThings.length).toBe 3
