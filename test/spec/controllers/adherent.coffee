'use strict'

describe 'Controller: AdherentCtrl', ->

  # load the controller's module
  beforeEach module 'ludikafeeAdminApp'

  AdherentCtrl = {}

  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    AdherentCtrl = $controller 'AdherentCtrl', {
      # place here mocked dependencies
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(AdherentCtrl.awesomeThings.length).toBe 3
