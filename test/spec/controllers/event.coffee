'use strict'

describe 'Controller: EventCtrl', ->

  # load the controller's module
  beforeEach module 'ludikafeeAdminApp'

  EventCtrl = {}

  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    EventCtrl = $controller 'EventCtrl', {
      # place here mocked dependencies
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(EventCtrl.awesomeThings.length).toBe 3
