'use strict'

describe 'Controller: PlacePlaceUpdateCtrl', ->

  # load the controller's module
  beforeEach module 'ludikafeeAdminApp'

  PlacePlaceUpdateCtrl = {}

  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    PlacePlaceUpdateCtrl = $controller 'PlacePlaceUpdateCtrl', {
      # place here mocked dependencies
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(PlacePlaceUpdateCtrl.awesomeThings.length).toBe 3
