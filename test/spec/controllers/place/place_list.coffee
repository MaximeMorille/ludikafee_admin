'use strict'

describe 'Controller: PlacePlaceListCtrl', ->

  # load the controller's module
  beforeEach module 'ludikafeeAdminApp'

  PlacePlaceListCtrl = {}

  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    PlacePlaceListCtrl = $controller 'PlacePlaceListCtrl', {
      # place here mocked dependencies
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(PlacePlaceListCtrl.awesomeThings.length).toBe 3
