'use strict'

describe 'Controller: PlacePlaceCreateCtrl', ->

  # load the controller's module
  beforeEach module 'ludikafeeAdminApp'

  PlacePlaceCreateCtrl = {}

  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    PlacePlaceCreateCtrl = $controller 'PlacePlaceCreateCtrl', {
      # place here mocked dependencies
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(PlacePlaceCreateCtrl.awesomeThings.length).toBe 3
