'use strict'

###*
 # @ngdoc overview
 # @name ludikafeeAdminApp
 # @description
 # # ludikafeeAdminApp
 #
 # Main module of the application.
###
angular
  .module 'ludikafeeAdminApp', [
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'angular-jwt',
    'LocalStorageModule'
  ]
  .config ($stateProvider, $urlRouterProvider, $locationProvider, $resourceProvider, SessionProvider, localStorageServiceProvider) ->
    $urlRouterProvider.otherwise('/')
    $resourceProvider.defaults.stripTrailingSlashes = false

    $locationProvider.html5Mode(true)

    localStorageServiceProvider.setPrefix('ludikafee_admin')

    $urlRouterProvider.rule(($injector, $location) ->
      session = $injector.get('Session')
      if !session?
        session = $injector.invoke(SessionProvider.$get)

      path = $location.path()

      if path == '/login'
        session.destroy()
        return
      if !session.isAuthorized()
        return '/login'
    )

    $stateProvider
      .state('login', {
        url: '/login'
        templateUrl: 'views/login.html'
        controller: 'LoginCtrl'
        controllerAs: 'login'
      })
      .state('home', {
        url: '/'
        templateUrl: 'views/home.html'
      })
      .state('home.adherents', {
        templateUrl: 'views/adherent/adherent_layout.html'
      })
      .state('home.adherents.list', {
        url: 'adherents'
        templateUrl: 'views/adherent/adherent_list.html'
        controller: 'AdherentCtrl'
        controllerAs: 'adherent'
        resolve: {
          adherentObj: (adherents, $stateParams) ->
            return adherents.index()
        }
      })
      .state('home.adherents.create', {
        url: 'adherents/new'
        onEnter: ['$uibModal', '$state', ($uibModal, $state) ->
          $uibModal.open({
            templateUrl: 'views/adherent/adherent_create.html'
            controller: 'AdherentCtrl'
            controllerAs: 'adherent'
            resolve: {
              adherentObj: () ->
                return {}
            }
          })
        ]
      })
      .state('home.adherents.update', {
        url: 'adherents/:id'
        templateUrl: 'views/adherent/adherent_update.html'
        controller: 'AdherentCtrl'
        controllerAs: 'adherent'
        resolve: {
          adherentObj: (adherents, $stateParams) ->
            return adherents.get({id: $stateParams.id})
        }
      })
      .state('home.events', {
        url: 'events'
        templateUrl: 'views/event/event.html'
        controller: 'EventCtrl'
        controllerAs: 'event'
      })
      .state('home.event', {
        url: 'events/:id'
        templateUrl: 'views/event/event.html'
        controller: 'EventCtrl'
        controllerAs: 'event'
      })
      .state('home.places', {
        url: 'places'
        templateUrl: 'views/place/place.html'
        controller: 'PlaceCtrl'
        controllerAs: 'place'
      })
      .state('home.place', {
        url: 'places/:id'
        templateUrl: 'views/place/place.html'
        controller: 'PlaceCtrl'
        controllerAs: 'place'
      })
  .run(($rootScope) ->
    $rootScope.$on('$stateChangeError', console.log.bind(console))
  )
