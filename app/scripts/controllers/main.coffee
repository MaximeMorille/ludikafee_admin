'use strict'

###*
 # @ngdoc function
 # @name ludikafeeAdminApp.controller:MainCtrl
 # @description
 # # MainCtrl
 # Controller of the ludikafeeAdminApp
###
angular.module 'ludikafeeAdminApp'
  .controller 'MainCtrl', ->
    @awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
    return
