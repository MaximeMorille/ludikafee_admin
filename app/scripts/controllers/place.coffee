'use strict'

###*
 # @ngdoc function
 # @name ludikafeeAdminApp.controller:PlaceCtrl
 # @description
 # # PlaceCtrl
 # Controller of the ludikafeeAdminApp
###
angular.module 'ludikafeeAdminApp'
  .controller 'PlaceCtrl', ->
    @awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
    return
