'use strict'

###*
 # @ngdoc function
 # @name ludikafeeAdminApp.controller:LoginCtrl
 # @description
 # # LoginCtrl
 # Controller of the ludikafeeAdminApp
###
angular.module('ludikafeeAdminApp')
  .controller('LoginCtrl', ($scope, AuthService, Session, $state) ->
    $scope.credentials = {
      username: ''
      password: ''
    }

    $scope.login = (credentials) ->
      AuthService.login(credentials)
        .then(
          (response) ->
            $state.go('home.adherents.list')
          (response) ->
            # response.[config|data|headers|status|statusText]
            console.log(response)
        )
  )
