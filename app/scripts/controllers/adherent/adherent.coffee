'use strict'

###*
 # @ngdoc function
 # @name ludikafeeAdminApp.controller:AdherentCtrl
 # @description
 # # AdherentCtrl
 # Controller of the ludikafeeAdminApp
###
angular.module('ludikafeeAdminApp')
  .controller('AdherentCtrl', ($scope, adherents, adherentObj) ->
    $scope.adherentObj = adherentObj

    $scope.createAdherent = () ->
      adherents.save(
        $scope.adherentObj,
        ($state) ->
          $scope.$close(() ->
            $state.go('home.adherents.list')
          )
        ,
        (resp) ->
          console.log($scope)
      )

    $scope.updateAdherent = () ->
      adherents.update({id: $scope.adherentObj.id}, $scope.adherentObj)

    $scope.deleteAdherent = (id) ->
      adherents.delete({id: id})
  )
