'use strict'

###*
 # @ngdoc provider
 # @name ludikafeeAdminApp.session
 # @description
 # # session
 # Service in the ludikafeeAdminApp.
###
angular.module('ludikafeeAdminApp')
  .provider('Session', () ->

    @$get = [
      'jwtHelper',
      '$http',
      'localStorageService',
      'API_KEY',
      (jwtHelper, $http, localStorageService, API_KEY) ->
        Session = {};

        Session.create = (token) ->
          payload = jwtHelper.decodeToken(token)

          @user = {
            id: payload.user_id
            name: payload.username
            email: payload.email
          }
          @token = token

          localStorageService.set('token', token)

          _setAuthHeader(token)
          Session.callNextRefresh()

        Session.updateToken = (newToken, oldToken) ->
          if oldToken?
            if @token == oldToken
              @token = newToken

              _setAuthHeader(@token)
          else
            @create(newToken)

        Session.refreshToken = () ->
          data = {
            token: Session.token
          }

          $http
            .post(API_KEY.url + '/api-token-refresh/', data)
            .success((data, status, headers, config) =>
              Session.updateToken(data.token, Session.token)
              Session.callNextRefresh()
            )
            .error((data, status, headers, config) ->
              Session.destroy()
            )

        Session.callNextRefresh = () ->
          date = jwtHelper.getTokenExpirationDate(@token)
          refreshDate = new Date(date.setMinutes(date.getMinutes() - 1))

          if Session.refreshTokenTimeout?
            clearTimeout(Session.refreshTokenTimeout)
          Session.refreshTokenTimeout = setTimeout(@refreshToken, refreshDate.valueOf() - Date.now())

        Session.destroy = () ->
          @user = undefined
          @token = undefined

          _setAuthHeader(null)

        Session.isAuthorized = () ->
          if @user?
            return true
          isAuthenticated = @_loadSessionFromStorage()
          return isAuthenticated

        Session._loadSessionFromStorage = () ->
          token = localStorageService.get('token')
          if token? && !jwtHelper.isTokenExpired(token)
            @create(token)
            return true

          return false

        _setAuthHeader = (token) ->
          if token?
            if $http.defaults.headers.common?
              $http.defaults.headers.common.Authorization = 'JWT ' + token
            else
              $http.defaults.headers.common = {
                Authorization: 'JWT ' + token
              }
          else
            if $http.defaults.headers.common?
              delete $http.defaults.headers.common.Authorization

        return Session
    ]

    return
  )
