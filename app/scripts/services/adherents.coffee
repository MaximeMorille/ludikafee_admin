'use strict'

###*
 # @ngdoc factory
 # @name ludikafeeAdminApp.adherents
 # @description
 # # adherents
 # Service in the ludikafeeAdminApp.
###
adherentsServices = angular.module('ludikafeeAdminApp')
                      .service('adherentsServices', ['ngResource'])

adherentsServices.factory('adherents', ['$resource', 'API_KEY',
  ($resource, API_KEY) ->
    return $resource(API_KEY.url + '/adherent/:id/', {},
      {
        index: {
          method: 'GET',
          url: API_KEY.url + '/adherent/'
          isArray: true,
          headers: {
            'Content-Type': 'application/json'
          }
        },
        create: {
          method: 'POST',
          url: API_KEY.url + '/adherent/'
          headers: {
            'Content-Type': 'application/json'
          }
        },
        update: {
          method: 'PUT'
          headers: {
            'Content-Type': 'application/json'
          }
        }
      }
    )
  ])
