'use strict'

###*
 # @ngdoc service
 # @name ludikafeeAdminApp.places
 # @description
 # # places
 # Service in the ludikafeeAdminApp.
###
angular.module 'ludikafeeAdminApp'
  .service 'places', ->
    # AngularJS will instantiate a singleton by calling "new" on this function
