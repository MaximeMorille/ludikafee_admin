'use strict'

###*
 # @ngdoc service
 # @name ludikafeeAdminApp.events
 # @description
 # # events
 # Service in the ludikafeeAdminApp.
###
angular.module 'ludikafeeAdminApp'
  .service 'events', ->
    # AngularJS will instantiate a singleton by calling "new" on this function
