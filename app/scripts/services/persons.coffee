'use strict'

###*
 # @ngdoc service
 # @name ludikafeeAdminApp.persons
 # @description
 # # persons
 # Service in the ludikafeeAdminApp.
###
angular.module 'ludikafeeAdminApp'
  .service 'persons', ->
    # AngularJS will instantiate a singleton by calling "new" on this function
