'use strict'

###*
 # @ngdoc service
 # @name ludikafeeAdminApp.apiKey
 # @description
 # # apiKey
 # Constant in the ludikafeeAdminApp.
###
angular.module('ludikafeeAdminApp')
  .constant('API_KEY', {
    # url: 'http://ludikafee-api'
    url: 'http://127.0.0.1:8000'
  })
